const config = {
  testEnvironment: 'node',
  verbose: true,
  collectCoverage: true,
  collectCoverageFrom: ['src/!(server)/!(routes)/!(schemas)/*.js'],
  moduleFileExtensions: ['js'],
}

module.exports = config

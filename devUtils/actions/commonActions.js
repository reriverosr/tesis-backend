const fs = require('fs')

const createApiRouteIfMissing = (apiName) => {
  const actions = []
  if (!fs.existsSync(`src/app/${apiName}/routes.js`)) {
    actions.push(
      ...[
        {
          type: 'add',
          path: 'src/app/index.js',
          templateFile: 'devUtils/templates/api/createRouteApp.hbs',
          skipIfExists: true,
        },
        {
          type: 'add',
          path: `src/app/${apiName}/routes.js`,
          templateFile: 'devUtils/templates/api/routes.hbs',
        },
        {
          type: 'append',
          path: 'src/app/index.js',
          templateFile: 'devUtils/templates/api/appRoutesModify.hbs',
          pattern: 'Here are the APIs with the prefixes',
          data: {
            api: apiName,
            prefix: 'v1',
          },
        },
        {
          type: 'append',
          path: 'src/app/index.js',
          templateFile: 'devUtils/templates/api/appRoutesModifyImports.hbs',
          pattern: 'Here are the imports',
        },
        {
          type: 'eslint',
          path: ['src/app/index.js', `src/app/${apiName}/routes.js`],
        },
      ],
    )
  }
  return actions
}

module.exports = {
  createApiRouteIfMissing,
}

const _ = require('lodash')
const fs = require('fs')
const {createApiRouteIfMissing} = require('./devUtils/actions/commonActions')

module.exports = async function (plop) {
  await plop.load('plop-action-eslint')
  // controller generator

  const getApisFromApplication = () => {
    try {
      if (!fs.existsSync('src/app')) throw new Error('app folder does not exist')
      const apis = fs
        .readdirSync('src/app', {withFileTypes: true})
        .filter((dirent) => dirent.isDirectory())
        .map((dirent) => dirent.name)
      if (!apis) throw new Error('apis not found')
      return apis
    } catch (err) {
      console.error(err)
      return ['test']
    }
  }

  plop.setGenerator('Create new API', {
    description: 'application api logic',
    prompts: [
      {
        type: 'input',
        name: 'api',
        message: 'What is the API name. Example: users. ',
      },
      {
        type: 'input',
        name: 'prefix',
        message: 'prefix name please. Example: "v1", "v1/beta"',
      },
    ],
    actions: function (data) {
      const actions = []
      actions.push(
        ...[
          {
            type: 'add',
            path: 'src/app/index.js',
            templateFile: 'devUtils/templates/api/createRouteApp.hbs',
            skipIfExists: true,
          },
          {
            type: 'add',
            path: 'src/app/{{lowerCase api}}/routes.js',
            templateFile: 'devUtils/templates/api/routes.hbs',
          },
          {
            type: 'append',
            path: 'src/app/index.js',
            templateFile: 'devUtils/templates/api/appRoutesModify.hbs',
            pattern: 'Here are the APIs with the prefixes',
          },
          {
            type: 'append',
            path: 'src/app/index.js',
            templateFile: 'devUtils/templates/api/appRoutesModifyImports.hbs',
            pattern: 'Here are the imports',
          },
          {
            type: 'eslint',
            path: ['src/app/index.js', 'src/app/{{lowerCase api}}/routes.js'],
          },
        ],
      )
      return actions
    },
  })
  plop.setGenerator('Create endpoint', {
    description: 'application endpoint logic',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'What is the endpoint handler name. Example: getUsers',
        validate(value) {
          if (!value) return 'Please enter a valid handler name'
          return true
        },
      },
      {
        type: 'list',
        name: 'api',
        message: 'Select parent API for endpoint',
        choices: getApisFromApplication(),
      },
      {
        type: 'list',
        choices: ['GET', 'POST', 'PUT', 'DELETE', 'PATCH'],
        name: 'method',
        message: 'Select http method for the endpoint',
      },
      {
        type: 'input',
        name: 'url',
        message: 'What is the route for the endpoint. Example: /:userId',
        validate(value) {
          if (!value) return 'Please enter a valid route for the endpoint'
          return true
        },
      },
    ],
    actions: function (data) {
      const actions = []

      actions.push(
        ...createApiRouteIfMissing(data.api),
        ...[
          {
            type: 'add',
            path: 'src/app/{{lowerCase api}}/routes.js',
            templateFile: 'devUtils/templates/api/routes.hbs',
            skipIfExists: true,
          },
          {
            type: 'addMany',
            destination: 'src/app/{{lowerCase api}}/handlers/',
            globOptions: {onlyFiles: false},
            base: 'devUtils/templates/endpoint/handlers/',
            stripExtensions: ['hbs'],
            templateFiles: 'devUtils/templates/endpoint/handlers/**/*/',
            abortOnFail: true,
          },
          {
            type: 'addMany',
            destination: 'src/app/{{lowerCase api}}/services/',
            globOptions: {onlyFiles: false},
            base: 'devUtils/templates/endpoint/services/',
            stripExtensions: ['hbs'],
            templateFiles: 'devUtils/templates/endpoint/services/**/*/',
            abortOnFail: true,
          },
          {
            type: 'addMany',
            destination: 'src/app/{{lowerCase api}}/repositories/',
            globOptions: {onlyFiles: false},
            base: 'devUtils/templates/endpoint/repositories/',
            stripExtensions: ['hbs'],
            templateFiles: 'devUtils/templates/endpoint/repositories/**/*/',
            abortOnFail: true,
          },
          {
            type: 'add',
            path: 'src/app/{{lowerCase api}}/schemas/{{lowerCase name}}.js',
            templateFile: 'devUtils/templates/endpoint/schema/schema.hbs',
          },
          {
            type: 'append',
            path: 'src/app/{{lowerCase api}}/routes.js',
            templateFile: 'devUtils/templates/endpoint/routes/modifyRoutes.hbs',
            pattern: 'Here are the routes from the API',
          },
          {
            type: 'append',
            path: 'src/app/{{lowerCase api}}/routes.js',
            templateFile: 'devUtils/templates/endpoint/routes/modifyImports.hbs',
            pattern: 'Here are the imports',
          },
          {
            type: 'eslint',
            path: [
              'src/app/{{lowerCase api}}/handlers/{{lowerCase name}}.js',
              'src/app/{{lowerCase api}}/handlers/__tests__/{{lowerCase name}}.test.js',
              'src/app/{{lowerCase api}}/services/{{lowerCase name}}.js',
              'src/app/{{lowerCase api}}/services/__tests__/{{lowerCase name}}.test.js',
              'src/app/{{lowerCase api}}/repositories/{{lowerCase name}}.js',
              'src/app/{{lowerCase api}}/repositories/__tests__/{{lowerCase name}}.test.js',
              'src/app/{{lowerCase api}}/schemas/{{lowerCase name}}.js',
              'src/app/{{lowerCase api}}/routes.js',
            ],
          },
        ],
      )
      return actions
    },
  })
}

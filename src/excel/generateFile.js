const fs = require('fs')
const XlsxTemplate = require('xlsx-template')
const xlsx = require('xlsx')
const path = require('path')

fs.readFile(path.join(__dirname, 'template.xlsx'), (err, data) => {
  console.log(data)
  const template = new XlsxTemplate(data)
  const sheetNumber = 1
  // const values = {
  //   extractDate: new Date(),
  //   dates: [new Date('2013-06-01'), new Date('2013-06-02'), new Date('2013-06-03')],
  //   people: [
  //     {name: 'John Smith', age: 20},
  //     {name: 'Bob Johnson', age: 22},
  //   ],
  // }
  const values = {
    quizzes: ['test'],
    questions: ['question 1', 'question 2'],
    answers: ['test answer'],
  }

  template.substitute(sheetNumber, values)
  const result = template.generate()
  fs.writeFileSync(path.join(__dirname, 'generateFile.xlsx'), result, 'binary')
})

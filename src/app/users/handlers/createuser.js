const makeCreateUserService = require('../services/createuser')

async function createUserHandler(request, reply) {
  const createUserService = makeCreateUserService(this)
  const response = await createUserService(request)
  return reply.code(200).send({data: response})
}

module.exports = createUserHandler

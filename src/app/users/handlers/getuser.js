const makeGetuserService = require('../services/getuser')

async function getUserHandler(request, reply) {
  const getUserService = makeGetuserService(this)
  const response = await getUserService(request)
  return reply.code(200).send({data: response})
}

module.exports = getUserHandler

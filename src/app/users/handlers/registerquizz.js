const makeRegisterQuizzService = require('../services/registerquizz')

async function registerQuizzHandler(request, reply) {
  const registerQuizzService = makeRegisterQuizzService(this)
  const response = await registerQuizzService(request)
  return reply.code(200).send({data: response})
}

module.exports = registerQuizzHandler

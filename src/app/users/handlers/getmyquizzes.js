const makeGetMyQuizzesService = require('../services/getmyquizzes')

async function getMyQuizzesHandler(request, reply) {
  const getMyQuizzesService = makeGetMyQuizzesService(this)
  const response = await getMyQuizzesService(request)
  return reply.code(200).send({data: response})
}

module.exports = getMyQuizzesHandler

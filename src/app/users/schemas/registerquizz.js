const registerQuizzSchema = {
  tags: ['users'],
  headers: {
    type: 'object',
  },
  params: {
    type: 'object',
    properties: {
      userId: {type: 'string'},
    },
  },
  body: {
    type: 'object',
    properties: {
      quizzId: {type: 'string'},
      name: {type: 'string'},
      email: {type: 'string'},
      attempts: {type: 'number', default: 1},
      score: {type: 'number'},
      extendedQuizzData: {
        type: 'array',
        items: {
          type: 'object',
          properties: {
            id: {type: 'string'},
            answer: {type: 'string'},
            isCorrect: {type: 'boolean'},
            currentQuestion: {type: 'number'},
          },
        },
      },
    },
  },
  response: {
    200: {
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            id: {type: 'string'},
          },
        },
      },
    },
  },
}

module.exports = registerQuizzSchema

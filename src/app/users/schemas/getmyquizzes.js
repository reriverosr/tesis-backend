const getMyQuizzesSchema = {
  tags: ['users'],
  headers: {
    type: 'object',
  },
  params: {
    type: 'object',
    properties: {
      userId: {type: 'string'},
    },
  },
  query: {
    type: 'object',
    properties: {
      includes: {type: 'string', default: 'questions'},
    },
  },
  response: {
    200: {
      type: 'object',
      properties: {
        data: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              id: {type: 'string'},
              name: {type: 'string'},
              userId: {type: 'string'},
              title: {type: 'string'},
              description: {type: 'string'},
              scale: {type: 'number'},
              maxScore: {type: 'number'},
              minApprove: {type: 'number'},
              maxAttempts: {type: 'number'},
              users: {
                type: 'array',
                items: {
                  type: 'object',
                  properties: {
                    score: {type: 'number'},
                    attempts: {type: 'number'},
                    user: {
                      type: 'object',
                      properties: {
                        id: {type: 'string'},
                        name: {type: 'string'},
                        email: {type: 'string'},
                      },
                    },
                  },
                },
              },
              questions: {
                type: 'array',
                items: {
                  type: 'object',
                  properties: {
                    id: {type: 'string'},
                    title: {type: 'string'},
                    answers: {
                      type: 'array',
                      items: {
                        type: 'object',
                        properties: {
                          id: {type: 'string'},
                          text: {type: 'string'},
                          isCorrect: {type: 'boolean'},
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    },
  },
}

module.exports = getMyQuizzesSchema

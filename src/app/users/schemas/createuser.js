const createUserSchema = {
  tags: ['users'],
  headers: {
    type: 'object',
  },
  body: {
    type: 'object',
    required: ['userId'],
    properties: {
      userId: {type: 'string'},
      name: {type: 'string'},
      email: {type: 'string'},
    },
  },
  response: {
    200: {
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            id: {type: 'string'},
          },
        },
      },
    },
  },
}

module.exports = createUserSchema

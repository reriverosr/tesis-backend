const getUserSchema = {
  tags: ['users'],
  headers: {
    type: 'object',
  },
  params: {
    type: 'object',
    properties: {
      userId: {type: 'string'},
    },
  },
  response: {
    200: {
      type: 'object',
      properties: {
        data: {
          type: 'object',
          additionalProperties: true,
          properties: {
            id: {type: 'string'},
            name: {type: 'string'},
            email: {type: 'string'},
          },
        },
      },
    },
  },
}

module.exports = getUserSchema

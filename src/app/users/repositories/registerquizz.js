function registerQuizzRepository(fastify) {
  // Here we have to put the configurations to make requests to external services
  return async (request) => {
    // Logic to make requests
    const {userId} = request.params
    const {quizzId, score, attempts, extendedQuizzData} = request.body
    const registerQuizzUser = await fastify.prisma.user.update({
      where: {
        id: userId,
      },
      data: {
        quizzes: {
          upsert: {
            where: {
              userId_quizzId: {
                userId,
                quizzId,
              },
            },
            create: {
              score,
              maxScore: score,
              attempts,
              extendedQuizzData,
              quizz: {
                connect: {
                  id: quizzId,
                },
              },
            },
            update: {
              score,
              extendedQuizzData,
              attempts: {
                increment: 1,
              },
            },
          },
        },
      },
      include: {
        quizzes: true,
      },
    })
    const currentQuizz = registerQuizzUser.quizzes.find(
      (quizz) => quizz.quizzId === quizzId,
    )
    if (currentQuizz.score <= score) {
      await fastify.prisma.user.update({
        where: {
          id: userId,
        },
        data: {
          quizzes: {
            update: {
              where: {
                userId_quizzId: {
                  userId,
                  quizzId,
                },
              },
              data: {
                maxScore: score,
              },
            },
          },
        },
      })
    }

    return registerQuizzUser
  }
}

module.exports = registerQuizzRepository

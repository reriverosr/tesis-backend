function getMyQuizzesRepository(fastify) {
  // Here we have to put the configurations to make requests to external services
  return async (request) => {
    // Logic to make requests
    const {userId} = request.params
    const {includes} = request.query
    const includeArray = includes.split(',').filter(Boolean)
    const quizzes = await fastify.prisma.quizz.findMany({
      where: {
        userId: userId,
      },
      ...(includeArray.length > 0 && {
        include: {
          ...(includeArray.includes('questions') && {
            questions: {
              include: {
                answers: true,
              },
            },
          }),
          ...(includeArray.includes('users') && {
            users: {
              include: {
                user: true,
              },
            },
          }),
        },
      }),
    })
    return quizzes
  }
}

module.exports = getMyQuizzesRepository

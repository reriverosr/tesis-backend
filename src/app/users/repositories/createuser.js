function createUserRepository(fastify) {
  // Here we have to put the configurations to make requests to external services
  return async (request) => {
    // Logic to make requests
    const {userId, email, name} = request.body
    const createUser = await fastify.prisma.user.upsert({
      where: {
        id: userId,
      },
      update: {
        email,
        name,
      },
      create: {
        id: userId,
        email,
        name,
      },
    })
    return createUser
  }
}

module.exports = createUserRepository

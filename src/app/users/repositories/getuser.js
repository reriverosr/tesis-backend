function getUserRepository(fastify) {
  // Here we have to put the configurations to make requests to external services
  return async (request) => {
    // Logic to make requests
    const {userId} = request.params
    const user = await fastify.prisma.user.findUnique({
      where: {
        id: userId,
      },
      include: {
        quizzes: true,
      },
    })
    return user
  }
}

module.exports = getUserRepository

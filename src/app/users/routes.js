// Here are the imports
const getMyQuizzesHandler = require('./handlers/getmyquizzes')
const getMyQuizzesSchema = require('./schemas/getmyquizzes')
const registerQuizzHandler = require('./handlers/registerquizz')
const registerQuizzSchema = require('./schemas/registerquizz')
const createUserHandler = require('./handlers/createuser')
const createUserSchema = require('./schemas/createuser')
const getUserHandler = require('./handlers/getuser')
const getUserSchema = require('./schemas/getuser')

module.exports = async (fastify) => {
  // Here are the routes from the API
  fastify.route({
    method: 'GET',
    url: '/:userId/quizzes',
    handler: getMyQuizzesHandler,
    schema: getMyQuizzesSchema,
  })
  fastify.route({
    method: 'POST',
    url: '/:userId/quizz',
    handler: registerQuizzHandler,
    schema: registerQuizzSchema,
  })
  fastify.route({
    method: 'POST',
    url: '/',
    handler: createUserHandler,
    schema: createUserSchema,
  })
  fastify.route({
    method: 'GET',
    url: '/:userId',
    handler: getUserHandler,
    schema: getUserSchema,
  })
}

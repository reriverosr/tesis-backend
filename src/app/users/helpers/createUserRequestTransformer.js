exports.createUserRequestTransformer = (request) => {
  const {userId} = request.params
  const {name, email} = request.body
  return {
    ...request,
    body: {
      userId,
      email,
      name,
    },
  }
}

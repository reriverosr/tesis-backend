const makeCreateUserRepository = require('../repositories/createuser')

function createUserService(fastify) {
  const createUserRepository = makeCreateUserRepository(fastify)
  return async (request) => {
    const response = await createUserRepository(request)
    return response
  }
}

module.exports = createUserService

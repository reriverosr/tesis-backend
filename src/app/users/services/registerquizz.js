const makeRegisterQuizzRepository = require('../repositories/registerquizz')
const makeCreateUserRepository = require('../repositories/createuser')
const {createUserRequestTransformer} = require('../helpers/createUserRequestTransformer')

function registerQuizzService(fastify) {
  const registerQuizzRepository = makeRegisterQuizzRepository(fastify)
  const createUserIfNotExists = makeCreateUserRepository(fastify)
  return async (request) => {
    const createUserRequest = createUserRequestTransformer(request)
    await createUserIfNotExists(createUserRequest)
    const response = await registerQuizzRepository(request)
    return response
  }
}

module.exports = registerQuizzService

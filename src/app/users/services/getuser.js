const makeGetuserRepository = require('../repositories/getuser')

function getUserService(fastify) {
  const getUserRepository = makeGetuserRepository(fastify)
  return async (request) => {
    const response = await getUserRepository(request)
    return response
  }
}

module.exports = getUserService

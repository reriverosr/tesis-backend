const makeGetMyQuizzesRepository = require('../repositories/getmyquizzes')

function getMyQuizzesService(fastify) {
  const getMyQuizzesRepository = makeGetMyQuizzesRepository(fastify)
  return async (request) => {
    const response = await getMyQuizzesRepository(request)
    return response
  }
}

module.exports = getMyQuizzesService

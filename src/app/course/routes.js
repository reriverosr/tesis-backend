// Here are the imports
const getAnalyticsHandler = require('./handlers/getanalytics')
const getAnalyticsSchema = require('./schemas/getanalytics')
const getQuizzesHandler = require('./handlers/getquizzes')
const getQuizzesSchema = require('./schemas/getquizzes')
const getCourseHandler = require('./handlers/getcourse')
const getCourseSchema = require('./schemas/getcourse')
const deleteCourseHandler = require('./handlers/deletecourse')
const deleteCourseSchema = require('./schemas/deletecourse')
const updateCourseHandler = require('./handlers/updatecourse')
const updateCourseSchema = require('./schemas/updatecourse')
const createCourseHandler = require('./handlers/createcourse')
const createCourseSchema = require('./schemas/createcourse')

module.exports = async (fastify) => {
  // Here are the routes from the API
  fastify.route({
    method: 'GET',
    url: '/:courseId/analytics',
    handler: getAnalyticsHandler,
    schema: getAnalyticsSchema,
  })
  fastify.route({
    method: 'GET',
    url: '/:courseId/quizzes',
    handler: getQuizzesHandler,
    schema: getQuizzesSchema,
  })
  fastify.route({
    method: 'GET',
    url: '/:courseId',
    handler: getCourseHandler,
    schema: getCourseSchema,
  })
  fastify.route({
    method: 'DELETE',
    url: '/:courseId',
    handler: deleteCourseHandler,
    schema: deleteCourseSchema,
  })
  fastify.route({
    method: 'PATCH',
    url: '/:courseId',
    handler: updateCourseHandler,
    schema: updateCourseSchema,
  })
  fastify.route({
    method: 'POST',
    url: '/',
    handler: createCourseHandler,
    schema: createCourseSchema,
  })
}

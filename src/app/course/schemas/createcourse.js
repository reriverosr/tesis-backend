const createCourseSchema = {
  tags: ['course'],
  headers: {
    type: 'object',
  },
  body: {
    type: 'object',
    required: ['id', 'name'],
    properties: {
      id: {type: 'number'},
      name: {type: 'string'},
    },
  },
  response: {
    200: {
      type: 'object',
      required: ['id', 'name'],
      properties: {
        id: {type: 'number'},
        name: {type: 'string'},
      },
    },
  },
}

module.exports = createCourseSchema

const updateCourseSchema = {
  tags: ['course'],
  headers: {
    type: 'object',
  },
  params: {
    type: 'object',
    properties: {
      courseId: {type: 'number'},
    },
  },
  body: {
    type: 'object',
    required: ['name'],
    properties: {
      name: {type: 'string'},
    },
  },
  response: {
    200: {
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            id: {type: 'string'},
          },
        },
      },
    },
  },
}

module.exports = updateCourseSchema

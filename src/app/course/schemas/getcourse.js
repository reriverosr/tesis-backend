const getCourseSchema = {
  tags: ['course'],
  headers: {
    type: 'object',
  },
  params: {
    type: 'object',
    properties: {
      courseId: {type: 'number'},
    },
  },
  response: {
    200: {
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            id: {type: 'number'},
            name: {type: 'string'},
          },
        },
      },
    },
  },
}

module.exports = getCourseSchema

const getAnalyticsSchema = {
  tags: ['course'],
  headers: {
    type: 'object',
  },
  params: {
    type: 'object',
    properties: {
      courseId: {type: 'number'},
    },
  },
  response: {
    200: {
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            id: {type: 'number'},
            name: {type: 'string'},
            quizzes: {
              type: 'array',
              items: {
                type: 'object',
                properties: {
                  id: {type: 'string'},
                  name: {type: 'string'},
                  description: {type: 'string'},
                  scale: {type: 'number'},
                  maxScore: {type: 'number'},
                  minApprove: {type: 'number'},
                  maxAttempts: {type: 'number'},
                  author: {type: 'string'},
                  questions: {
                    type: 'array',
                    items: {
                      type: 'object',
                      properties: {
                        id: {type: 'string'},
                        title: {type: 'string'},
                        answers: {
                          type: 'array',
                          items: {
                            type: 'object',
                            properties: {
                              id: {type: 'string'},
                              text: {type: 'string'},
                              isCorrect: {type: 'boolean'},
                            },
                          },
                        },
                      },
                    },
                  },
                  createdAt: {type: 'string'},
                  updatedAt: {type: 'string'},
                  users: {
                    type: 'array',
                    items: {
                      type: 'object',
                      properties: {
                        studentId: {type: 'string'},
                        name: {type: 'string'},
                        email: {type: 'string'},
                        score: {type: 'number'},
                        maxScore: {type: 'number'},
                        attempts: {type: 'number'},
                        firstTry: {type: 'string'},
                        lastTry: {type: 'string'},
                        lastAttempt: {
                          type: 'array',
                          items: {
                            type: 'object',
                            properties: {
                              id: {type: 'string'},
                              answer: {type: 'string'},
                              isCorrect: {type: 'boolean'},
                              currentQuestion: {type: 'number'},
                            },
                          },
                        },
                      },
                    },
                  },
                },
              },
            },
            results: {
              type: 'array',
              items: {
                type: 'object',
                properties: {
                  quizzId: {type: 'string'},
                  users: {
                    type: 'array',
                    items: {
                      type: 'object',
                      properties: {
                        studentId: {type: 'string'},
                        name: {type: 'string'},
                        email: {type: 'string'},
                        score: {type: 'number'},
                        attempts: {type: 'number'},
                        maxScore: {type: 'number'},
                        approved: {type: 'boolean'},
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    },
  },
}

module.exports = getAnalyticsSchema

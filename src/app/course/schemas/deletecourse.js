const deleteCourseSchema = {
  tags: ['course'],
  headers: {
    type: 'object',
  },
  params: {
    type: 'object',
    properties: {
      courseId: {type: 'number'},
    },
  },
  body: {
    type: 'object',
  },
  response: {
    204: {
      type: 'object',
    },
  },
}

module.exports = deleteCourseSchema

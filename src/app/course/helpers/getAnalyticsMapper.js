const objectMapper = require('object-mapper')

function getResultsForUsers(response) {
  const {quizzes} = response

  return quizzes
    .map((quizz) => {
      const {id, minApprove, users = []} = quizz
      if (!users.length) return {quizzId: id, users}
      return {
        quizzId: id,
        users: users.map(({userId, attempts, maxScore, score, user}) => {
          return {
            studentId: userId,
            name: user.name,
            email: user.email,
            score,
            maxScore,
            attempts,
            approved: maxScore >= minApprove,
          }
        }),
      }
    })
    .flat(1)
}

function handlerMapper(src) {
  const mapper = {
    id: ['id', {key: 'results', transform: () => getResultsForUsers(src)}],
    name: 'name',
    'quizzes[].id': 'quizzes[].id',
    'quizzes[].name': 'quizzes[].name',
    'quizzes[].description': 'quizzes[].description',
    'quizzes[].scale': 'quizzes[].scale',
    'quizzes[].maxScore': 'quizzes[].maxScore',
    'quizzes[].userId': 'quizzes[].author',
    'quizzes[].minApprove': 'quizzes[].minApprove',
    'quizzes[].questions': 'quizzes[].questions',
    'quizzes[].users[].userId': 'quizzes[].users[].studentId',
    'quizzes[].users[].userId': 'quizzes[].users[].studentId',
    'quizzes[].users[].score': 'quizzes[].users[].score',
    'quizzes[].users[].maxScore': 'quizzes[].users[].maxScore',
    'quizzes[].users[].attempts': 'quizzes[].users[].attempts',
    'quizzes[].users[].createdAt': 'quizzes[].users[].firstTry',
    'quizzes[].users[].updatedAt': 'quizzes[].users[].lastTry',
    'quizzes[].users[].user.name': 'quizzes[].users[].name',
    'quizzes[].users[].user.email': 'quizzes[].users[].email',
    'quizzes[].users[].extendedQuizzData': 'quizzes[].users[].lastAttempt',
  }
  return objectMapper(src, mapper)
}

module.exports = {
  handlerMapper,
}

const makeUpdateCourseRepository = require('../repositories/updatecourse')

function updateCourseService(fastify) {
  const updateCourseRepository = makeUpdateCourseRepository(fastify)
  return async (request) => {
    const response = await updateCourseRepository(request)
    return response
  }
}

module.exports = updateCourseService

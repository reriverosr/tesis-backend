const makeGetQuizzesRepository = require('../repositories/getquizzes')

function getQuizzesService(fastify) {
  const getQuizzesRepository = makeGetQuizzesRepository(fastify)
  return async (request) => {
    const response = await getQuizzesRepository(request)
    return response
  }
}

module.exports = getQuizzesService

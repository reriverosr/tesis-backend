const makeGetCourseRepository = require('../repositories/getcourse')

function getCourseService(fastify) {
  const getCourseRepository = makeGetCourseRepository(fastify)
  return async (request) => {
    const response = await getCourseRepository(request)
    return response
  }
}

module.exports = getCourseService

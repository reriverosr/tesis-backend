const makeGetAnalyticsRepository = require('../repositories/getanalytics')

function getAnalyticsService(fastify) {
  const getAnalyticsRepository = makeGetAnalyticsRepository(fastify)
  return async (request) => {
    const response = await getAnalyticsRepository(request)
    return response
  }
}

module.exports = getAnalyticsService

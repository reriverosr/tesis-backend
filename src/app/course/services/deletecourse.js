const makeDeleteCourseRepository = require('../repositories/deletecourse')

function deleteCourseService(fastify) {
  const deleteCourseRepository = makeDeleteCourseRepository(fastify)
  return async (request) => {
    const response = await deleteCourseRepository(request)
    return response
  }
}

module.exports = deleteCourseService

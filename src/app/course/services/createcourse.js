const makeCreateCourseRepository = require('../repositories/createcourse')

function createCourseService(fastify) {
  const createCourseRepository = makeCreateCourseRepository(fastify)
  return async (request) => {
    const response = await createCourseRepository(request)
    return response
  }
}

module.exports = createCourseService

function getCourseRepository(fastify) {
  // Here we have to put the configurations to make requests to external services
  return async (request) => {
    // Logic to make requests
    const {courseId} = request.params
    const course = await fastify.prisma.course.findUnique({
      where: {
        id: courseId,
      },
    })
    return course
  }
}

module.exports = getCourseRepository

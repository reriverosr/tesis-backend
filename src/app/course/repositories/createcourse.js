function createCourseRepository(fastify) {
  // Here we have to put the configurations to make requests to external services
  return async (request) => {
    // Logic to make requests
    const {name, id} = request.body
    const course = await fastify.prisma.course.upsert({
      where: {
        id,
      },
      update: {
        ...(!!name && {name: `Course ${name}`}),
      },
      create: {
        id,
        name: `Course ${!!name ? name : id}`,
      },
    })
    return course
  }
}

module.exports = createCourseRepository

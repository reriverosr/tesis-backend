function deleteCourseRepository(fastify) {
  // Here we have to put the configurations to make requests to external services
  return async (request) => {
    const {courseId} = request.params
    const deleteCourse = await fastify.prisma.course.delete({
      where: {
        id: courseId,
      },
    })
    return deleteCourse
  }
}

module.exports = deleteCourseRepository

function updateCourseRepository(fastify) {
  // Here we have to put the configurations to make requests to external services
  return async (request) => {
    // Logic to make requests
    const {courseId} = request.params
    const {name} = request.body
    const updateCourse = await fastify.prisma.course.update({
      where: {
        id: courseId,
      },
      data: {
        name,
      },
    })
    return updateCourse
  }
}

module.exports = updateCourseRepository

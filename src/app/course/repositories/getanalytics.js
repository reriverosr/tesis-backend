const {handlerMapper} = require('../helpers/getAnalyticsMapper')

function getAnalyticsRepository(fastify) {
  // Here we have to put the configurations to make requests to external services
  return async (request) => {
    // Logic to make requests
    const {courseId} = request.params

    const quizzes = await fastify.prisma.course.findUnique({
      where: {
        id: courseId,
      },
      include: {
        quizzes: {
          include: {
            questions: {
              include: {
                answers: true,
              },
            },
            users: {
              include: {
                user: true,
              },
            },
          },
        },
      },
    })

    console.log('** quizzes', JSON.stringify(quizzes))
    return handlerMapper(quizzes)
  }
}

module.exports = getAnalyticsRepository

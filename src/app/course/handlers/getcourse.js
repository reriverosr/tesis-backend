const makeGetCourseService = require('../services/getcourse')

async function getCourseHandler(request, reply) {
  const getCourseService = makeGetCourseService(this)
  const response = await getCourseService(request)
  return reply.code(200).send({data: response})
}

module.exports = getCourseHandler

const makeDeleteCourseService = require('../services/deletecourse')

async function deleteCourseHandler(request, reply) {
  const deleteCourseService = makeDeleteCourseService(this)
  const response = await deleteCourseService(request)
  return reply.code(200).send({data: response})
}

module.exports = deleteCourseHandler

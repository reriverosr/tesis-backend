const makeUpdateCourseService = require('../services/updatecourse')

async function updateCourseHandler(request, reply) {
  const updateCourseService = makeUpdateCourseService(this)
  const response = await updateCourseService(request)
  return reply.code(200).send({data: response})
}

module.exports = updateCourseHandler

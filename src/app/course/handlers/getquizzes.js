const makeGetQuizzesService = require('../services/getquizzes')

async function getQuizzesHandler(request, reply) {
  const getQuizzesService = makeGetQuizzesService(this)
  const response = await getQuizzesService(request)
  return reply.code(200).send({data: response})
}

module.exports = getQuizzesHandler

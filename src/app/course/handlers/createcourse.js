const makeCreateCourseService = require('../services/createcourse')

async function createCourseHandler(request, reply) {
  const createCourseService = makeCreateCourseService(this)
  const response = await createCourseService(request)
  return reply.code(200).send({data: response})
}

module.exports = createCourseHandler

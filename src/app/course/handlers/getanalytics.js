const makeGetAnalyticsService = require('../services/getanalytics')

async function getAnalyticsHandler(request, reply) {
  const getAnalyticsService = makeGetAnalyticsService(this)
  const response = await getAnalyticsService(request)
  return reply.code(200).send({data: response})
}

module.exports = getAnalyticsHandler

// Here are the imports
const updateAnswerHandler = require('./handlers/updateanswer')
const updateAnswerSchema = require('./schemas/updateanswer')
const deleteAnswerHandler = require('./handlers/deleteanswer')
const deleteAnswerSchema = require('./schemas/deleteanswer')
const createAnswerHandler = require('./handlers/createanswer')
const createAnswerSchema = require('./schemas/createanswer')
const updateQuestionHandler = require('./handlers/updatequestion')
const updateQuestionSchema = require('./schemas/updatequestion')
const deleteQuestionHandler = require('./handlers/deletequestion')
const deleteQuestionSchema = require('./schemas/deletequestion')
const createQuestionHandler = require('./handlers/createquestion')
const createQuestionSchema = require('./schemas/createquestion')
const getQuizzHandler = require('./handlers/getquizz')
const getQuizzSchema = require('./schemas/getquizz')
const updateQuizzHandler = require('./handlers/updatequizz')
const updateQuizzSchema = require('./schemas/updatequizz')
const deleteQuizzHandler = require('./handlers/deletequizz')
const deleteQuizzSchema = require('./schemas/deletequizz')
const createQuizzHandler = require('./handlers/createquizz')
const createQuizzSchema = require('./schemas/createquizz')

module.exports = async (fastify) => {
  // Here are the routes from the API
  fastify.route({
    method: 'PATCH',
    url: '/:quizzId/question/:questionId/answer/:answerId',
    handler: updateAnswerHandler,
    schema: updateAnswerSchema,
  })
  fastify.route({
    method: 'DELETE',
    url: '/:quizzId/question/:questionId/answer/:answerId',
    handler: deleteAnswerHandler,
    schema: deleteAnswerSchema,
  })
  fastify.route({
    method: 'POST',
    url: '/:quizzId/question/:questionId/answer',
    handler: createAnswerHandler,
    schema: createAnswerSchema,
  })
  fastify.route({
    method: 'PATCH',
    url: '/:quizzId/question/:questionId',
    handler: updateQuestionHandler,
    schema: updateQuestionSchema,
  })
  fastify.route({
    method: 'DELETE',
    url: '/:quizzId/question/:questionId',
    handler: deleteQuestionHandler,
    schema: deleteQuestionSchema,
  })
  fastify.route({
    method: 'POST',
    url: '/:quizzId/question',
    handler: createQuestionHandler,
    schema: createQuestionSchema,
  })
  fastify.route({
    method: 'GET',
    url: '/:quizzId',
    handler: getQuizzHandler,
    schema: getQuizzSchema,
  })
  fastify.route({
    method: 'PATCH',
    url: '/:quizzId',
    handler: updateQuizzHandler,
    schema: updateQuizzSchema,
  })
  fastify.route({
    method: 'DELETE',
    url: '/:quizzId',
    handler: deleteQuizzHandler,
    schema: deleteQuizzSchema,
  })
  fastify.route({
    method: 'POST',
    url: '/',
    handler: createQuizzHandler,
    schema: createQuizzSchema,
  })
}

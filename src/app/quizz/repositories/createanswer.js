function createAnswerRepository(fastify) {
  // Here we have to put the configurations to make requests to external services
  return async (request) => {
    // Logic to make requests
    const {questionId} = request.params
    const {text, isCorrect} = request.body
    const createAnswer = await fastify.prisma.answer.create({
      data: {
        text,
        isCorrect,
        questionId,
      },
    })
    return createAnswer
  }
}

module.exports = createAnswerRepository

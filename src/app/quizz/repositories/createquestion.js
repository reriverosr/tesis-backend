function createQuestionRepository(fastify) {
  // Here we have to put the configurations to make requests to external services
  return async (request) => {
    // Logic to make requests
    const {quizzId} = request.params
    const {title} = request.body
    const question = await fastify.prisma.question.create({
      data: {
        title,
        quizzId,
      },
    })
    return question
  }
}

module.exports = createQuestionRepository

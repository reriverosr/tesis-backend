function deleteAnswerRepository(fastify) {
  // Here we have to put the configurations to make requests to external services
  return async (request) => {
    // Logic to make requests
    const {answerId} = request.params
    const deleteAnswer = await fastify.prisma.answer.delete({
      where: {
        id: answerId,
      },
    })
    return deleteAnswer
  }
}

module.exports = deleteAnswerRepository

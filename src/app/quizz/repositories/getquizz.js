function getQuizzRepository(fastify) {
  // Here we have to put the configurations to make requests to external services
  return async (request) => {
    // Logic to make requests
    const {quizzId} = request.params
    const {includes} = request.query
    const includeArray = includes.split(',').filter(Boolean)
    const quizz = await fastify.prisma.quizz.findUnique({
      where: {
        id: quizzId,
      },
      ...(includeArray.length > 0 && {
        include: {
          ...(includeArray.includes('questions') && {
            questions: {
              include: {
                answers: true,
              },
            },
          }),
          ...(includeArray.includes('users') && {
            users: {
              include: {
                user: true,
              },
            },
          }),
        },
      }),
    })
    return quizz
  }
}

module.exports = getQuizzRepository

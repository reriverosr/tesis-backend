function deleteQuestionRepository(fastify) {
  // Here we have to put the configurations to make requests to external services
  return async (request) => {
    // Logic to make requests
    const {questionId} = request.params
    const deleteQuestion = await fastify.prisma.question.delete({
      where: {
        id: questionId,
      },
    })
    return deleteQuestion
  }
}

module.exports = deleteQuestionRepository

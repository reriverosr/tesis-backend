function deleteQuizzRepository(fastify) {
  // Here we have to put the configurations to make requests to external services
  return async (request) => {
    // Logic to make requests
    const {quizzId} = request.params
    const deleteQuizz = await fastify.prisma.quizz.delete({
      where: {
        id: quizzId,
      },
    })
    return deleteQuizz
  }
}

module.exports = deleteQuizzRepository

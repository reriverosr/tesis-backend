function createQuizzRepository(fastify) {
  // Here we have to put the configurations to make requests to external services
  return async (request) => {
    // Logic to make requests
    const {
      name,
      courseId,
      scale,
      maxScore,
      minApprove,
      maxAttempts,
      title,
      description,
      userId,
      questions,
    } = request.body
    const quizz = await fastify.prisma.quizz.create({
      data: {
        name,
        courseId,
        scale,
        maxAttempts,
        maxScore,
        minApprove,
        title,
        description,
        userId,
        ...(questions.length && {
          questions: {
            create: questions.map(({title, order, answers}) => ({
              title,
              order,
              answers: {
                create: answers.map(({text, order, isCorrect}) => ({
                  text,
                  order,
                  isCorrect,
                })),
              },
            })),
          },
        }),
      },
      include: {
        questions: true,
      },
    })
    return quizz
  }
}

module.exports = createQuizzRepository

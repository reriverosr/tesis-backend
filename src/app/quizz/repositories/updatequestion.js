function updateQuestionRepository(fastify) {
  // Here we have to put the configurations to make requests to external services
  return async (request) => {
    // Logic to make requests
    const {questionId} = request.params
    const {title} = request.body
    const updateQuestion = await fastify.prisma.question.update({
      where: {
        id: questionId,
      },
      data: {
        title,
      },
    })
    return updateQuestion
  }
}

module.exports = updateQuestionRepository

function updateAnswerRepository(fastify) {
  // Here we have to put the configurations to make requests to external services
  return async (request) => {
    // Logic to make requests
    const {answerId} = request.params
    const {text, isCorrect} = request.body
    const updateAnswer = await fastify.prisma.answer.update({
      where: {
        id: answerId,
      },
      data: {
        text,
        isCorrect,
      },
    })
    return updateAnswer
  }
}

module.exports = updateAnswerRepository

const _ = require('lodash')
const {getQuestionsQuery} = require('../helpers/getQuestionsQuery')
const {getTransactions} = require('../helpers/getTransactions')

function updateQuizzRepository(fastify) {
  // Here we have to put the configurations to make requests to external services
  return async (request) => {
    // Logic to make requests
    const {quizzId} = request.params
    const {
      name,
      scale,
      maxScore,
      minApprove,
      maxAttempts,
      title,
      description,
      questions,
    } = request.body

    const getQuizzQuestions = await fastify.prisma.question.findMany({
      where: {
        quizzId,
      },
      include: {
        answers: true,
      },
    })

    const instructions = getQuestionsQuery(questions, getQuizzQuestions)

    const transactions = getTransactions(fastify, instructions, quizzId)

    const [transactionsResponse] = await fastify.prisma.$transaction([...transactions])

    const updateQuizzResponse = await fastify.prisma.quizz.update({
      where: {
        id: quizzId,
      },
      data: {
        scale,
        name,
        maxScore,
        minApprove,
        maxAttempts,
        title,
        description,
      },
      include: {
        questions: {
          include: {answers: true},
        },
      },
    })

    return updateQuizzResponse
  }
}

module.exports = updateQuizzRepository

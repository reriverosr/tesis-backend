exports.createCourseTransformer = (request) => {
  return {
    ...request,
    body: {
      id: request.body.courseId,
    },
  }
}

const _ = require('lodash')

function isQuestionUnchanged(q1, q2) {
  return q1.title === q2.title
}
function isAnswersUnchanged(q1, q2) {
  const newQ1 = _.pick(q1, ['id', 'text', 'isCorrect'])
  const newQ2 = _.pick(q2, ['id', 'text', 'isCorrect'])
  return _.isEqual(newQ1, newQ2)
}

const getQuestionsQuery = (questions, responseQuestions) => {
  //Assign questionId to each question if any
  _.forEach(questions, (question) =>
    _.forEach(question.answers, (answer) => _.assign(answer, {questionId: question.id})),
  )
  const orderedRequestQuestions = _.orderBy(questions, ['id'], ['asc'])
  const orderedResponseQuestions = _.orderBy(responseQuestions, ['id'], ['asc'])

  const requestAnswers = _.flatMap(
    orderedRequestQuestions,
    (question) => question.answers,
  )
  const responseAnswers = _.flatMap(
    orderedResponseQuestions,
    (question) => question.answers,
  )

  const newQuestions = _.filter(
    orderedRequestQuestions,
    (question) => !_.find(orderedResponseQuestions, ['id', question.id]),
  )
  const existingQuestions = _.filter(orderedRequestQuestions, (question) =>
    _.find(orderedResponseQuestions, ['id', question.id]),
  )
  const removedQuestions = _.filter(
    orderedResponseQuestions,
    (question) => !_.find(orderedRequestQuestions, ['id', question.id]),
  )

  const changedQuestions = _.differenceWith(
    existingQuestions,
    responseQuestions,
    isQuestionUnchanged,
  )
  const changedAnswers = _.differenceWith(
    requestAnswers,
    responseAnswers,
    isAnswersUnchanged,
  )

  const newAnswers = _.filter(
    requestAnswers,
    (answer) => !_.find(responseAnswers, ['id', answer.id]),
  )

  const removedAnswers = _.filter(
    responseAnswers,
    (answer) => !_.find(requestAnswers, ['id', answer.id]),
  )

  const newQuestionsQuery = _.map(newQuestions, (question) => ({
    title: question.title,
    order: question.order,
    answers: question.answers,
  }))
  const newAnswersQuery = _.map(
    _.filter(newAnswers, (answer) => Boolean(_.get(answer, 'questionId', null))),
    (answer) => ({
      questionId: answer.questionId,
      text: answer.text,
      order: answer.order,
      isCorrect: answer.isCorrect,
    }),
  )
  const existingQuestionsQuery = _.map(changedQuestions, (question) => ({
    title: question.title,
    order: question.order,
    id: question.id,
  }))
  const existingAnswersQuery = _.map(
    _.filter(changedAnswers, (answer) => Boolean(_.get(answer, 'id', null))),
    (answer) => ({
      questionId: answer.questionId,
      id: answer.id,
      text: answer.text,
      order: answer.order,
      isCorrect: answer.isCorrect,
    }),
  )
  const removedQuestionsQuery = _.map(removedQuestions, (question) => ({
    id: question.id,
  }))
  const removedAnswersQuery = _.map(removedAnswers, (answer) => ({
    id: answer.id,
  }))
  return {
    createInstructions: {
      questions: newQuestionsQuery,
      answers: newAnswersQuery,
    },
    updateInstructions: {
      questions: existingQuestionsQuery,
      answers: existingAnswersQuery,
    },
    deleteInstructions: {
      questions: removedQuestionsQuery,
      answers: removedAnswersQuery,
    },
  }
}

module.exports = {getQuestionsQuery}

const _ = require('lodash')

const getTransactions = (fastify, instructions, quizzId) => {
  const {createInstructions, updateInstructions, deleteInstructions} = instructions

  const transactions = []

  if (!_.isEmpty(deleteInstructions.answers)) {
    transactions.push(
      fastify.prisma.answer.deleteMany({
        where: {
          id: {
            in: deleteInstructions.answers.map((answer) => answer.id),
          },
        },
      }),
    )
  }
  if (!_.isEmpty(deleteInstructions.questions)) {
    transactions.push(
      fastify.prisma.question.deleteMany({
        where: {
          id: {
            in: deleteInstructions.questions.map((question) => question.id),
          },
        },
      }),
    )
  }

  if (!_.isEmpty(updateInstructions.answers)) {
    transactions.push(
      ...updateInstructions.answers.map(({id, text, order, isCorrect}) =>
        fastify.prisma.answer.update({
          where: {
            id,
          },
          data: {
            text,
            order,
            isCorrect,
          },
        }),
      ),
    )
  }
  if (!_.isEmpty(updateInstructions.questions)) {
    transactions.push(
      ...updateInstructions.questions.map(({id, order, title}) =>
        fastify.prisma.question.update({
          where: {
            id,
          },
          data: {
            title,
            order,
          },
        }),
      ),
    )
  }

  if (!_.isEmpty(createInstructions.answers)) {
    transactions.push(
      ...createInstructions.answers.map(({questionId, text, order, isCorrect}) =>
        fastify.prisma.question.update({
          where: {
            id: questionId,
          },
          data: {
            answers: {
              create: {
                text,
                order,
                isCorrect,
              },
            },
          },
        }),
      ),
    )
  }

  if (!_.isEmpty(createInstructions.questions)) {
    transactions.push(
      ...createInstructions.questions.map(({title, order, answers}) =>
        fastify.prisma.quizz.update({
          where: {
            id: quizzId,
          },
          data: {
            questions: {
              create: {
                title,
                order,
                answers: {
                  create: answers.map(({text, order, isCorrect}) => ({
                    text,
                    order,
                    isCorrect,
                  })),
                },
              },
            },
          },
        }),
      ),
    )
  }
  return transactions
}

module.exports = {getTransactions}

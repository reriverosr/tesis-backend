const makeUpdateQuestionService = require('../services/updatequestion')

async function updateQuestionHandler(request, reply) {
  const updateQuestionService = makeUpdateQuestionService(this)
  const response = await updateQuestionService(request)
  return reply.code(200).send({data: response})
}

module.exports = updateQuestionHandler

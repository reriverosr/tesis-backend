const makeCreateQuestionService = require('../services/createquestion')

async function createQuestionHandler(request, reply) {
  const createQuestionService = makeCreateQuestionService(this)
  const response = await createQuestionService(request)
  return reply.code(200).send({data: response})
}

module.exports = createQuestionHandler

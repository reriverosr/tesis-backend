const makeDeleteAnswerService = require('../services/deleteanswer')

async function deleteAnswerHandler(request, reply) {
  const deleteAnswerService = makeDeleteAnswerService(this)
  const response = await deleteAnswerService(request)
  return reply.code(200).send({data: response})
}

module.exports = deleteAnswerHandler

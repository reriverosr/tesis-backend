const makeDeleteQuestionService = require('../services/deletequestion')

async function deleteQuestionHandler(request, reply) {
  const deleteQuestionService = makeDeleteQuestionService(this)
  const response = await deleteQuestionService(request)
  return reply.code(200).send({data: response})
}

module.exports = deleteQuestionHandler

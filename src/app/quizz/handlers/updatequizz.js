const makeUpdateQuizzService = require('../services/updatequizz')

async function updateQuizzHandler(request, reply) {
  const updateQuizzService = makeUpdateQuizzService(this)
  const response = await updateQuizzService(request)
  return reply.code(200).send({data: response})
}

module.exports = updateQuizzHandler

const makeCreateQuizzService = require('../services/createquizz')

async function createQuizzHandler(request, reply) {
  const createQuizzService = makeCreateQuizzService(this)
  const response = await createQuizzService(request)
  return reply.code(200).send({data: response})
}

module.exports = createQuizzHandler

const makeGetQuizzService = require('../services/getquizz')

async function getQuizzHandler(request, reply) {
  const getQuizzService = makeGetQuizzService(this)
  const response = await getQuizzService(request)
  return reply.code(200).send({data: response})
}

module.exports = getQuizzHandler

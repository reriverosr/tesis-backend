const makeCreateAnswerService = require('../services/createanswer')

async function createAnswerHandler(request, reply) {
  const createAnswerService = makeCreateAnswerService(this)
  const response = await createAnswerService(request)
  return reply.code(200).send({data: response})
}

module.exports = createAnswerHandler

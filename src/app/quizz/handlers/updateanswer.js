const makeUpdateAnswerService = require('../services/updateanswer')

async function updateAnswerHandler(request, reply) {
  const updateAnswerService = makeUpdateAnswerService(this)
  const response = await updateAnswerService(request)
  return reply.code(200).send({data: response})
}

module.exports = updateAnswerHandler

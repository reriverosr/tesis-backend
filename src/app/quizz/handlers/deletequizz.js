const makeDeleteQuizzService = require('../services/deletequizz')

async function deleteQuizzHandler(request, reply) {
  const deleteQuizzService = makeDeleteQuizzService(this)
  const response = await deleteQuizzService(request)
  return reply.code(200).send({data: response})
}

module.exports = deleteQuizzHandler

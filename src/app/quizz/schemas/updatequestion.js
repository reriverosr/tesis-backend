const updateQuestionSchema = {
  tags: ['quizz'],
  headers: {
    type: 'object',
  },
  params: {
    type: 'object',
    properties: {
      quizzId: {type: 'string'},
      questionId: {type: 'string'},
    },
  },
  body: {
    type: 'object',
    required: ['title'],
    properties: {
      title: {type: 'string'},
    },
  },
  response: {
    200: {
      type: 'object',
    },
  },
}

module.exports = updateQuestionSchema

const createAnswerSchema = {
  tags: ['quizz'],
  headers: {
    type: 'object',
  },
  params: {
    type: 'object',
    properties: {
      quizzId: {type: 'string'},
      questionId: {type: 'string'},
    },
  },
  body: {
    type: 'object',
    properties: {
      text: {type: 'string'},
      isCorrect: {type: 'boolean'},
    },
  },
  response: {
    200: {
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            id: {type: 'string'},
          },
        },
      },
    },
  },
}

module.exports = createAnswerSchema

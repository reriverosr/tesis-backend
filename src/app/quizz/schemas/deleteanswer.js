const deleteAnswerSchema = {
  tags: ['quizz'],
  headers: {
    type: 'object',
  },
  params: {
    type: 'object',
    properties: {
      quizzId: {type: 'string'},
      questionId: {type: 'string'},
      answerId: {type: 'string'},
    },
  },
  body: {
    type: 'object',
  },
  response: {
    200: {
      type: 'object',
    },
  },
}

module.exports = deleteAnswerSchema

const createQuizzSchema = {
  tags: ['quizz'],
  headers: {
    type: 'object',
  },
  body: {
    type: 'object',
    required: ['name', 'title', 'userId', 'courseId'],
    properties: {
      name: {type: 'string'},
      title: {type: 'string'},
      description: {type: 'string'},
      userId: {type: 'string'},
      courseId: {type: 'number'},
      scale: {type: 'number'},
      maxScore: {type: 'number'},
      minApprove: {type: 'number'},
      maxAttempts: {type: 'number'},
      questions: {
        type: 'array',
        items: {
          type: 'object',
          properties: {
            title: {type: 'string'},
            order: {type: 'number'},
            answers: {
              type: 'array',
              items: {
                type: 'object',
                properties: {
                  text: {type: 'string'},
                  order: {type: 'number'},
                  isCorrect: {type: 'boolean'},
                },
              },
            },
          },
        },
      },
    },
  },
  response: {
    200: {
      type: 'object',
      required: ['data'],
      properties: {
        data: {
          type: 'object',
          properties: {
            id: {type: 'string'},
          },
        },
      },
    },
  },
}

module.exports = createQuizzSchema

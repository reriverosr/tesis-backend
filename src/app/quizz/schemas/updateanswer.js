const updateAnswerSchema = {
  tags: ['quizz'],
  headers: {
    type: 'object',
  },
  params: {
    type: 'object',
    properties: {
      quizzId: {type: 'string'},
      questionId: {type: 'string'},
      answerId: {type: 'string'},
    },
  },
  body: {
    type: 'object',
    properties: {
      text: {type: 'string'},
      isCorrect: {type: 'boolean'},
    },
  },
  response: {
    200: {
      type: 'object',
    },
  },
}

module.exports = updateAnswerSchema

const updateQuizzSchema = {
  tags: ['quizz'],
  headers: {
    type: 'object',
  },
  params: {
    type: 'object',
    properties: {
      quizzId: {type: 'string'},
    },
  },
  body: {
    type: 'object',
    properties: {
      name: {type: 'string'},
      title: {type: 'string'},
      description: {type: 'string'},
      userId: {type: 'string'},
      scale: {type: 'number'},
      maxScore: {type: 'number'},
      minApprove: {type: 'number'},
      maxAttempts: {type: 'number'},
      questions: {
        type: 'array',
        items: {
          type: 'object',
          properties: {
            id: {type: 'string', default: ''},
            order: {type: 'number'},
            title: {type: 'string'},
            answers: {
              type: 'array',
              items: {
                type: 'object',
                properties: {
                  id: {type: 'string'},
                  order: {type: 'number'},
                  text: {type: 'string'},
                  isCorrect: {type: 'boolean'},
                },
              },
            },
          },
        },
      },
    },
  },
  response: {
    200: {
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            id: {type: 'string'},
            name: {type: 'string'},
            title: {type: 'string'},
            description: {type: 'string'},
            userId: {type: 'string'},
            scale: {type: 'number'},
            maxScore: {type: 'number'},
            minApprove: {type: 'number'},
            maxAttempts: {type: 'number'},
            questions: {
              type: 'array',
              items: {
                type: 'object',
                properties: {
                  id: {type: 'string', default: ''},
                  title: {type: 'string'},
                  answers: {
                    type: 'array',
                    items: {
                      type: 'object',
                      properties: {
                        id: {type: 'string', default: ''},
                        text: {type: 'string'},
                        isCorrect: {type: 'boolean'},
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    },
  },
}

module.exports = updateQuizzSchema

const createQuestionSchema = {
  tags: ['quizz'],
  headers: {
    type: 'object',
  },
  params: {
    type: 'object',
    properties: {
      quizzId: {type: 'string'},
    },
  },
  body: {
    type: 'object',
    required: ['title'],
    properties: {
      title: {type: 'string'},
    },
  },
  response: {
    200: {
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            id: {type: 'string'},
          },
        },
      },
    },
  },
}

module.exports = createQuestionSchema

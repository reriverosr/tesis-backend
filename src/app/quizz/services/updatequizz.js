const makeUpdateQuizzRepository = require('../repositories/updatequizz')

function updateQuizzService(fastify) {
  const updateQuizzRepository = makeUpdateQuizzRepository(fastify)
  return async (request) => {
    const response = await updateQuizzRepository(request)
    return response
  }
}

module.exports = updateQuizzService

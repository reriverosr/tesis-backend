const makeGetQuizzRepository = require('../repositories/getquizz')

function getQuizzService(fastify) {
  const getQuizzRepository = makeGetQuizzRepository(fastify)
  return async (request) => {
    const response = await getQuizzRepository(request)
    return response
  }
}

module.exports = getQuizzService

const makeCreateQuizzRepository = require('../repositories/createquizz')
const makeCreateCourseRepository = require('../../course/repositories/createcourse')
const {createCourseTransformer} = require('../helpers/createCourseTransformer')

function createQuizzService(fastify) {
  const createQuizzRepository = makeCreateQuizzRepository(fastify)
  const createCourseIfNotExists = makeCreateCourseRepository(fastify)
  return async (request) => {
    const courseRequest = createCourseTransformer(request)
    await createCourseIfNotExists(courseRequest)
    const response = await createQuizzRepository(request)
    return response
  }
}

module.exports = createQuizzService

const makeCreateQuestionRepository = require('../repositories/createquestion')

function createQuestionService(fastify) {
  const createQuestionRepository = makeCreateQuestionRepository(fastify)
  return async (request) => {
    const response = await createQuestionRepository(request)
    return response
  }
}

module.exports = createQuestionService

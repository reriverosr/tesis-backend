const makeDeleteQuizzRepository = require('../repositories/deletequizz')

function deleteQuizzService(fastify) {
  const deleteQuizzRepository = makeDeleteQuizzRepository(fastify)
  return async (request) => {
    const response = await deleteQuizzRepository(request)
    return response
  }
}

module.exports = deleteQuizzService

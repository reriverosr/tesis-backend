const makeCreateAnswerRepository = require('../repositories/createanswer')

function createAnswerService(fastify) {
  const createAnswerRepository = makeCreateAnswerRepository(fastify)
  return async (request) => {
    const response = await createAnswerRepository(request)
    return response
  }
}

module.exports = createAnswerService

const makeDeleteQuestionRepository = require('../repositories/deletequestion')

function deleteQuestionService(fastify) {
  const deleteQuestionRepository = makeDeleteQuestionRepository(fastify)
  return async (request) => {
    const response = await deleteQuestionRepository(request)
    return response
  }
}

module.exports = deleteQuestionService

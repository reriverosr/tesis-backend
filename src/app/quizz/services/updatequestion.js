const makeUpdateQuestionRepository = require('../repositories/updatequestion')

function updateQuestionService(fastify) {
  const updateQuestionRepository = makeUpdateQuestionRepository(fastify)
  return async (request) => {
    const response = await updateQuestionRepository(request)
    return response
  }
}

module.exports = updateQuestionService

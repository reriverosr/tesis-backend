const makeUpdateAnswerRepository = require('../repositories/updateanswer')

function updateAnswerService(fastify) {
  const updateAnswerRepository = makeUpdateAnswerRepository(fastify)
  return async (request) => {
    const response = await updateAnswerRepository(request)
    return response
  }
}

module.exports = updateAnswerService

const makeDeleteAnswerRepository = require('../repositories/deleteanswer')

function deleteAnswerService(fastify) {
  const deleteAnswerRepository = makeDeleteAnswerRepository(fastify)
  return async (request) => {
    const response = await deleteAnswerRepository(request)
    return response
  }
}

module.exports = deleteAnswerService

// Here are the imports
const courseRoutes = require('./course/routes')
const quizzRoutes = require('./quizz/routes')
const usersRoutes = require('./users/routes')

module.exports = async (fastify) => {
  // Here are the APIs with the prefixes
  fastify.register(courseRoutes, {prefix: 'v1/course'})
  fastify.register(quizzRoutes, {prefix: 'v1/quizz'})
  fastify.register(usersRoutes, {prefix: 'v1/users'})
}

const Fastify = require('fastify')
const routes = require('./src/app')
const autoload = require('fastify-autoload')
const path = require('path')
const cors = require('@fastify/cors')

const fastify = Fastify({
  logger: true,
})

const registerPlugins = () => {
  fastify.register(routes)
  fastify.register(autoload, {
    dir: path.join(__dirname, 'src/plugins'),
  })
  fastify.register(cors, (instance) => {
    return (req, callback) => {
      const corsOptions = {
        // This is NOT recommended for production as it enables reflection exploits
        origin: true,
      }

      // do not include CORS headers for requests from localhost
      if (/^localhost$/m.test(req.headers.origin)) {
        corsOptions.origin = false
      }

      // callback expects two parameters: error and options
      callback(null, corsOptions)
    }
  })
}

const start = async () => {
  registerPlugins()
  await fastify.ready()
  try {
    await fastify.listen(4444, '0.0.0.0')
  } catch (err) {
    fastify.log.error(err)
    process.exit(1)
  }
}

start()
